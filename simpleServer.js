'use strict';
var express = require('express');
var app = express();
var xml = '<?xml version="1.0" encoding="UTF-8"?><Response><Play loop="10">https://api.twilio.com/cowbell.mp3</Play></Response>';

app.get('/', function (req, res) {
  res.header('Content-Type','text/xml').send(xml);
});

var server = app.listen(3000, function () {
  var host = server.address().address;
  var port = server.address().port;

  console.log('Example app listening at http://%s:%s', host, port);
});